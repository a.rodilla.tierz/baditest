const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

// const mongopool = require('./infrastructure/mongoManager.js');
// require('./models/Passanger');
const dotenv = require('dotenv');

const app = express();

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || `mongodb://localhost:27017/node-react-starter`);

dotenv.config({path: '.env'});
app.use(bodyParser.json());
app.set('clientMongo', mongoose);
require('./routes/userRoutes')(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`app running on port ${PORT}`)
});
