// const mongoose = require('mongoose');
// const builderFlySeats = require('../bussines/BuilderFlySeats');
const fliesManagement = require('../bussines/fliesManagement.js');
const passangerLogin = require('../bussines/passangerLogin.js');

module.exports = (app) => {
  app.get(`/api/passangers`, async (req, res) => {
    console.error(req.params);
    return res.status(200).send();
  });


  app.post(`/api/checkPassanger`, async (req, res) => {
    console.error(req.body);
    const responseLogin = await passangerLogin.checkPassanger(app.get('clientMongo'), req.body.name, req.body.password);
    console.error('responseLogin: ', responseLogin);
    return res.status(200).send(responseLogin);
  });

  app.get(`/api/newFly`, async (req, res) => {
    return res.status(200).send(await fliesManagement.createNewFly(app.get('clientMongo')));
  });

  app.get(`/api/listFlies`, async (req, res) => {
    return res.status(200).send(await fliesManagement.listFlies(app.get('clientMongo')));
  });
}
