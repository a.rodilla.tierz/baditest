const passangerManager = require('../infrastructure/passangerManager.js')

module.exports.checkPassanger = async (clientMongo, name, password) => {
  const managerObj = new passangerManager(clientMongo);
  const listPassanger = await managerObj.searchPassanger(name, password);
  console.error('listPassanger: ', listPassanger);
  if (listPassanger.length === 0) {
    return await managerObj.createNewPassanger(name, password);
  } else if (listPassanger.length === 1) {
    return listPassanger;
  } else {
    return undefined;
  }
}
