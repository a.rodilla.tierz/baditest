const seat_status = require('../references/seatReference.js');
const seatManager = require('../infrastructure/seatsManager.js');

module.exports.BuilderFlySeats = async (mongoose) => {
  const managerObj = new seatManager(mongoose);
  console.error('managerObj: ', managerObj);
  const resp = await managerObj.generateFlySeats();
  console.error('resp: ', resp);
  return resp;
};

// module.exports = BuilderFlySeats;
