const fliesManager = require('../infrastructure/fliesManager.js');
const builderFlySeats = require('./seatsBussiness.js');
// await builderFlySeats.BuilderFlySeats(app.get('clientMongo'), 1);


module.exports.createNewFly = async (clientMongo) => {
  const pileSeats = await builderFlySeats.BuilderFlySeats(clientMongo);
  const managerObj = new fliesManager(clientMongo);
  managerObj.generateFly(new Date(), pileSeats);
}

module.exports.listFlies = async (clientMongo) => {
  const managerObj = new fliesManager(clientMongo);
  const listFlies = await managerObj.getListFlie();
  console.error('listFlies: ', listFlies[3].seats);
  return listFlies;
}
