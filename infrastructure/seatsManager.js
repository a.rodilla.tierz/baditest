// const Seat = mongoose.model('seat');
const seat_status = require('../references/seatReference.js');
const planeSpecs = require('../references/planespecs.js');

const SeatManager = class {
  constructor(mongoose) {
    require('../models/Flies');
    // console.error('mongoose: ', mongoose);
    this.mainSeat = mongoose.model('seat');
  }

  async insertSeat(row, word) {
    return await this.mainSeat.create({
                                        row: row,
                                        word: word,
                                        status: seat_status.SEAT_STATUS.AVALIABLE
                                      });
  }


  async generateFlySeats() {
    console.error('planeSpecs: ', planeSpecs);
    const pileNewSeats = []
    for (let rowNum = 0; rowNum < planeSpecs.STANDART_PLANE.ROWS; rowNum++) {
      for (const {nodeWord, nodeXXX} in planeSpecs.STANDART_PLANE.WORDS) {
        console.error('nodeWord: ', nodeWord);
        console.error('nodeXXX: ', nodeXXX);
        const nodeSeat = await this.insertSeat(rowNum, nodeWord);
        console.error('nodeSeat: ', nodeSeat);
        pileNewSeats.push(nodeSeat);
      }
    }

    // console.error('pileNewSeats: ', pileNewSeats);
    return pileNewSeats;
  }

  // async generateFlySeats() {
  //   console.error('planeSpecs: ', planeSpecs);
  //   const pileNewSeats = []
  //   for (let rowNum = 0; rowNum < planeSpecs.STANDART_PLANE.ROWS; rowNum++) {
  //     for (const nodeWord in planeSpecs.STANDART_PLANE.WORDS) {
  //       console.error('nodeWord: ', nodeWord);
  //       pileNewSeats.push({
  //                           row: rowNum,
  //                           word: nodeWord,
  //                           status: seat_status.SEAT_STATUS.AVALIABLE
  //                         });
  //     }
  //   }
  //
  //   // console.error('pileNewSeats: ', pileNewSeats);
  //   return pileNewSeats;
  // }
}

module.exports = SeatManager;
