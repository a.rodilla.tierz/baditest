const fly_status = require('../references/flyReference.js');

const FliesManager = class {
  constructor(mongoose) {
    require('../models/Flies');
    console.error('will create objet fly');
    this.mainFlies = mongoose.model('flies');
  }

  async insertFly(workDate, pileSeats) {
    return await this.mainFlies.create({
                                          fly_time: workDate,
                                          status: fly_status.AVALIABLE,
                                          seats: pileSeats
                                        });
  }

  async generateFly(workDate, pileSeats) {
    console.error('pileSeats: ', pileSeats);
    let newFly = undefined;
    try {
      newFly = await this.insertFly(workDate, pileSeats);
    } catch(e) {
      console.error('error at insert: ', e);
    }
    console.error('newFly: ', newFly);
    return newFly;
  }

  async getListFlie() {
    return await this.mainFlies.find();
  }
}

module.exports = FliesManager;
