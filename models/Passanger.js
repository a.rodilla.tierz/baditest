const mongoose = require('mongoose');
const {Schema} = mongoose;

const productSchema = new Schema({
  name: {
        type: String,
        required: true
    },
  password: {
        type: String,
        required: true
    },
  flies: [{
    fly_id: Number,
    status: Number
  }]
})

mongoose.model('passanger', productSchema);
