const mongoose = require('mongoose');
const {Schema} = mongoose;

const seatSchema = new Schema({
  row: Number,
  word: String,
  status: Number,
  user: Number
});

const flySchema = new Schema({
  fly_time: Date,
  status: Number,
  seats: [{
      type: mongoose.Schema.Types.ObjectId,
      ref: 'seat'
  }]
})

mongoose.model('seat', seatSchema);
mongoose.model('flies', flySchema);
